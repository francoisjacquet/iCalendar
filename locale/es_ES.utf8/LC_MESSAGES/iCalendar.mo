��          4      L       `   J   a   P   �   �  �   P   �  _   2                    Add classes to your Outlook, Google, Thunderbird, Android calendar (.ics). Add school events to your Outlook, Google, Thunderbird, Android calendar (.ics). Project-Id-Version: iCalendar plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:08+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Añade las clases a su calendario Outlook, Google, Thunderbird o Android (.ics). Añade los eventos de la escuela a su calendario Outlook, Google, Thunderbird o Android (.ics). 