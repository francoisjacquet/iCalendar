��          4      L       `   J   a   P   �   �  �   T   �  Z                       Add classes to your Outlook, Google, Thunderbird, Android calendar (.ics). Add school events to your Outlook, Google, Thunderbird, Android calendar (.ics). Project-Id-Version: iCalendar plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:09+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Adicione as aulas ao seu calendário Outlook, Google, Thunderbird ou Android (.ics). Adicione eventos escolares ao calendário do Outlook, Google, Thunderbird, Android (.ics). 